package course.live.email.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;

import com.amazonaws.services.simpleemail.model.*;
import course.live.common.Mail;
import course.live.common.MailProvider;

/*
* Implementación adaptada de https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-using-sdk-java.html
* */
public class AWSMailProvider implements MailProvider {

    public boolean send(Mail message) {

        String from = message.getFrom();
        String to = message.getTo();
        String body = message.getBody();
        String subject = message.getSubject();

        try {
            AmazonSimpleEmailService client =
                    AmazonSimpleEmailServiceClientBuilder.standard()
                            // Reemplazar US_EAST_1 con la región de AWS que estás usando para Amazon SES.
                            .withRegion(Regions.US_EAST_1).build();

            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(
                            new Destination().withToAddresses(to))
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withText(new Content()
                                            .withCharset("UTF-8").withData(body)))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(subject)))
                    .withSource(from);

            client.sendEmail(request);
            System.out.println("AWS - The email was sent.");
            return true;
        } catch (Exception ex) {
            System.err.println("AWS - The email was not sent. Error message: "  + ex.getMessage());
            return false;
        }


    }
}
