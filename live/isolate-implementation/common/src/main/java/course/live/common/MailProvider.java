package course.live.common;

public interface MailProvider {

    boolean send(Mail message);

}
