package course.live;

import course.live.business.ProductService;
import course.live.common.MailProvider;

public class Application {

    public static void main(String[] args) {

        ProductService service = new ProductService();

        //AWS
        //MailProvider provider = new course.live.email.aws.AWSMailProvider();

        //SendGrid
        //MailProvider provider = new course.live.email.sendgrid.SendGridMailProvider();

        //SparkPost
        MailProvider provider = new course.live.email.sparkpost.SparkPostMailProvider();


        service.setEmailProvider(provider);

        service.registerProduct("iPhone X", "Teléfonos móviles");

    }

}

