package course.live.business;

import course.live.common.Mail;
import course.live.common.MailProvider;

public class ProductService extends Service {

    public void registerProduct(String productName, String productCategory) {

        //...Realizar lógica de negocio para registrar el producto.

        MailProvider provider = getMailProvider();
        if(provider != null) {
            //Enviar mensaje notificando de creación del producto.
            String body = String.format("El producto '%s' de la categoria '%s' ha sido registrado exitosamente", productName, productCategory);

            String from = "FROM@CORREO.co";

            Mail message = new Mail(from, "PARA@CORREO.co", "Producto registrado", body);
            provider.send(message);
        }

    }

}
