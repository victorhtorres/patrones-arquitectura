package course.live.business;

import course.live.common.MailProvider;

public abstract class Service {

    private MailProvider provider;

    protected MailProvider getMailProvider() {
        return provider;
    }

    public void setEmailProvider(MailProvider provider){
        this.provider = provider;
    }

}
