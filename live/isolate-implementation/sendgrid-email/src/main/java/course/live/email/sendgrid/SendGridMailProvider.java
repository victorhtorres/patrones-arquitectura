package course.live.email.sendgrid;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import course.live.common.Mail;
import course.live.common.MailProvider;

import java.io.IOException;

public class SendGridMailProvider implements MailProvider {

    public boolean send(Mail message) {
        boolean result;
        try {
            SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");

            final String jsonRequestFormat ="{\"personalizations\":[" +
                                                "{\"to\":[{\"email\":\"%s\"}]," +
                                                "\"subject\":\"%s\"}]," +
                                                "\"from\":{\"email\":\"%s\"}," +
                                                "\"content\":[{\"type\":\"text/plain\", " +
                                                "\"value\": \"%s\"}]}";
            String jsonRequest = String.format(jsonRequestFormat, message.getTo(), message.getSubject(),
                                                message.getFrom(), message.getBody());
            request.setBody(jsonRequest);

            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());

            result = response.getStatusCode() == 200;

        } catch (IOException ex) {
            System.err.println("SendGrid - The email was not sent. Error message: "  + ex.getMessage());
            result = false;
        }

        return result;

    }

}
